from xml.etree.ElementTree import canonicalize
from django.db import models

# Create your models here.
class Watercraft(models.Model):
    class CraftType(models.TextChoices):
        SAILBOAT = 'sailboat', 'Yacht'
        MOTORBOAT = 'motorboat', 'Bateau à moteur'
        RIB = 'rib', 'Bateau semi-rigide'
        CATAMARAN = 'catamaran', 'Catamaran'
        SCHONNER = 'schooner', 'Goélette'
        JETSKI = 'jetski', 'Jet ski'
        HOUSEBOAT = 'houseboat', 'Péniche'
        YACHT = 'yacht', 'Yacht'
        DESTROYER = 'destroyer', 'Destroyer'
        TANKER = 'tanker', 'Navire-citerne'
    name = models.CharField()
    max_capacity = models.IntegerField()
    max_speed = models.DecimalField()
    engine_power = models.IntegerField()
    consumption = models.DecimalField()
    deposit = models.DecimalField()
    description = models.TextField()
    with_vhf = models.BooleanField()
    with_skipper = models.BooleanField()
    with_fuel = models.BooleanField()
    with_wifi = models.BooleanField()
    craft_type = models.CharField(choices=CraftType.choices)
    brand = models.CharField() marque
    version = models.CharField() modèle
    height = models.DecimalField() hauteur
    lenght = models.DecimalField() largeur
    draught = models.DecimalField() tirant d'eau
