from django.apps import AppConfig


class SailsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sails'
